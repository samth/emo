A simple command line emoji finder written in [Racket][racket]. Usage:

```console
> emo cat
Found 12 matches:
🐈‍⬛ 🐈 🐱 😾 😿 🙀 😽 😼 😻 😹 😸 😺
```

With descriptions:
```console
> emo -v cat
Found 12 matches:
🐈‍⬛   black cat
🐈   cat
🐱   cat face
😾   pouting cat
😿   crying cat
🙀   weary cat
😽   kissing cat
😼   cat with wry smile
😻   smiling cat with heart-eyes
😹   cat with tears of joy
😸   grinning cat with smiling eyes
😺   grinning cat
```

Note that your terminal may not display all emoji correctly; nevertheless copy-pasting them to a fully emoji-aware program (like Firefox) will work fine.

Currently the search is done on the description field of the [emoji-test.txt][emoji] published by unicode.org.

[emoji]: https://unicode.org/Public/emoji/14.0/emoji-test.txt
[racket]: https://racket-lang.org

# Installation

```console
> raco pkg install emo
```
